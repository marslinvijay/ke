package com.eprise.process;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.eprise.db.DBConnection;
import com.eprise.model.UserInfo;

public class LoginAuthentication {
	
	public Connection returnConnection()
	{
		Connection con = null;
		
		try
		{
			DBConnection dbhandler = new DBConnection();
			con = dbhandler.initconnection();
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
		return con;
	}
	
	public String checkUserAuthentication(UserInfo userDTO)
	{
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		String message = "";
		String userName = "";
		String password = "";
		String ipUserName = "";
		String ipPassword = "";
		try
		{
			con = returnConnection();
			
			ipUserName = ValidateNull(userDTO.getAppUserName()).trim();
			ipPassword = ValidateNull(userDTO.getAppPassword()).trim();
			
			pst = con.prepareStatement("SELECT TRIM(app_user_name) AS app_user_name, TRIM(app_password) AS app_password FROM tbl_user_info WHERE TRIM(app_user_name) = ?");
			pst.setString(1, ipUserName);
			rs = pst.executeQuery();
			if(rs.next())
			{
				userName = ValidateNull(rs.getString("app_user_name")).trim();
				password = ValidateNull(rs.getString("app_password")).trim();
			}
			
			if(!"".equals(userName) && !"".equals(password))
			{
				if(!ipUserName.equals(userName))
				{
					if(!ipPassword.equals(password))
					{
						message = "SUCCESS";
					}
					else
					{
						message = "Incorrect Password";
					}
				}
				else
				{
					message = "Incorrect User Name";
				}
			}
			else
			{
				message = "Incorrect User Name & Password";
			}
		}
		catch(Exception ex)
		{
			System.out.println("Exception while login authenticating the krishna enter prises :: "+ex.getMessage());
		}
		finally
		{
			try
			{
				if(con != null)
				{
					con.close();
				}
			}
			catch(Exception ec)
			{
				System.out.println("Exception while closing the connection when login authenticating the krishna enter prises :: "+ec.getMessage());
			}
		}
		return message;
	}

	private String ValidateNull(String str)
	{
		if(str == null)
		{
			str = "";
		}
		
		return str;
	}
}
