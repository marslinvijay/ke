package com.eprise.db;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.eprise.properties.ReadProperties;

public class DBConnection 
{
	  String strDriver;
	  String strUrl;
	  String strUsr;
	  String strPwd;
	  private Connection con;
	  
  public DBConnection()throws DBException
  {
	  strDriver= ReadProperties.getProperties("DB_Driver");
	  strUrl= ReadProperties.getProperties("DB_URL");
	  strUsr= ReadProperties.getProperties("DB_Username");
	  strPwd= ReadProperties.getProperties("DB_Password");
  }
  
  public Connection initconnection()throws SQLException,Exception
  {  
	  try
	  {
	     Class.forName(strDriver).newInstance();
	     con=DriverManager.getConnection(strUrl,strUsr,strPwd);	     
	  }
	  catch(SQLException sqlexp)
	  {
		  throw new DBException(sqlexp.getMessage());
      }
	  catch(Exception exp)
	  {
		  throw new DBException(exp.getMessage());
	  }
	  return con;  
  }
}
