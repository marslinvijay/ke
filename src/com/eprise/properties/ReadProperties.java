package com.eprise.properties;

import java.util.ResourceBundle;

import org.apache.log4j.Logger;

/**
 * 
 * This class uses to read the contents in property file.If user passes the key it matching that key and give values.
 */
public class ReadProperties {

	private static final Logger logger = Logger.getLogger(ReadProperties.class);
	public static final ResourceBundle resource = ResourceBundle.getBundle("eprise");
	
	/**
	 * 
	 * This method accepts the user given key and matches that then give the values of key.
	 * @param key - User Key
	 * @return - Key value
	 */
	public static String getProperties(String key)
	{
		return resource.getString(key);
	}
}
